let mobileMenu = document.querySelector('.mobile-menu')
let burgerButton = document.getElementById('burger-button')
let exitButton = document.getElementById('mobile-exit')
let mPortfolio = document.getElementById('m-portfolio')
let mServices = document.getElementById('m-services')
let mTechStack = document.getElementById('m-tech-stack')
let mTeam = document.getElementById('m-team')
let mContact = document.getElementById('m-contact')

burgerButton.addEventListener('click', () => {
    mobileMenu.classList.add('display-mobile-menu')
    setTimeout(() => {
        mobileMenu.classList.add('mobile-menu-transition')
    }, 2)
    setTimeout(() => {
        document.body.classList.add('no-scroll')
    }, 200)
})

exitButton.addEventListener('click', () => {
    mobileMenu.classList.remove('mobile-menu-transition')
    document.body.classList.remove('no-scroll')
})

mPortfolio.addEventListener('click', () => {
    mobileMenu.classList.remove('mobile-menu-transition')
    document.body.classList.remove('no-scroll')
})

mServices.addEventListener('click', () => {
    mobileMenu.classList.remove('mobile-menu-transition')
    document.body.classList.remove('no-scroll')
})

mTechStack.addEventListener('click', () => {
    mobileMenu.classList.remove('mobile-menu-transition')
    document.body.classList.remove('no-scroll')
})

mTeam.addEventListener('click', () => {
    mobileMenu.classList.remove('mobile-menu-transition')
    document.body.classList.remove('no-scroll')
})

mContact.addEventListener('click', () => {
    mobileMenu.classList.remove('mobile-menu-transition')
    document.body.classList.remove('no-scroll')
})