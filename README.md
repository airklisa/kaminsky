# Kaminsky

A sample company landing page website i designed and coded during my internship at **Bridgewater Labs**.

Font is Poppins from [Google Fonts](https://fonts.google.com/).
Stock images are from [Pexels](https://www.pexels.com/).
SVG background made using [BGjar](https://bgjar.com/).

Created by **Mihajlo Radić** - **November 2021**

[![Netlify Status](https://api.netlify.com/api/v1/badges/bd6aa967-d9d9-44b9-aebf-fccb363df190/deploy-status)](https://app.netlify.com/sites/mihajlo-kaminsky/deploys)